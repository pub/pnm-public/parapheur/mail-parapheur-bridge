declare module "pdf2json" {
  export default class PDFParser {
    on(eventName: "pdfParser_dataError", callback: (error: Error) => void): void;
    on(eventName: "pdfParser_dataReady", callback: (data: PDFParserData) => void): void;
    loadPDF(data: string): void;
  }

  export interface PDFParserData {
    Meta: Record<string, any>;
    Pages: PDFParserPage[];
  }

  export interface PDFParserPage {
    Width: number;
    Height: number;
    HLines: PDFParserLine[];
    VLines: PDFParserLine[];
    Fills: PDFParserFill[];
    Texts: PDFParserText[];
  }

  export interface PDFParserLine {
    x: number;
    y: number;
    w: number;
    l: number;
    oc: string;
  }

  export interface PDFParserFill {
    x: number;
    y: number;
    w: number;
    h: number;
    oc?: string;
    clr?: number;
  }

  export interface PDFParserText {
    x: number;
    y: number;
    w: number;
    sw: number;
    clr: number;
    A: "left" | "center" | "right";
    R: PDFParserTextRun[];
  }

  export interface PDFParserTextRun {
    T: string;
    S: number;
    TS: number[];
  }
}
