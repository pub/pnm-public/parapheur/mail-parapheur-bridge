// eslint-disable-next-line @typescript-eslint/no-var-requires
const winston = require("winston");

module.exports = {
  typeorm: {
    level: "error",
    transports: new winston.transports.Console(),
  },
  default: {
    level: (environment => {
      if (environment === "production") return "info";
      if (environment === "test") return "none";
      return "debug";
    })(process.env.NODE_ENV),
    transports: new winston.transports.Console(),
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.json(),
      winston.format.colorize({
        all: process.env.NODE_ENV !== "production",
        colors: {
          error: "red",
          info: "cyan",
          warn: "yellow",
          debug: "green",
        },
      })
    ),
  },
};
