import { AbstractNestCommand, Command } from "@pasta/cli";
import { Argv } from "yargs";
import { PdfService } from "../services/pdf.service";

@Command
export class GeneratePdfCommand extends AbstractNestCommand {
  name = "generate-pdf";
  description = "Generate a PDF file";

  arguments(yargs: Argv<{}>) {
    return yargs.option("file", {});
  }

  async main(argv: { file: string }): Promise<any> {
    const pdfService = this.nest.get(PdfService);
    await pdfService.generatePdf("Hello world", argv.file);
  }
}
