import { Type } from "class-transformer";
import { IsBoolean, IsNumber, IsOptional, IsString, ValidateNested } from "class-validator";

export class BridgeImapConf {
  @IsString()
  user: string;

  @IsString()
  @IsOptional()
  password?: string;

  @IsString()
  @IsOptional()
  passwordEnv?: string;

  @IsString()
  host: string;

  @IsNumber()
  port: number;

  @IsBoolean()
  @IsOptional()
  secure?: boolean;

  @IsNumber()
  @IsOptional()
  resyncDelay?: number;

  @IsBoolean()
  @IsOptional()
  rejectUnauthorized?: boolean;
}

export class BridgeAccount {
  @IsString()
  name: string;

  @Type(() => BridgeImapConf)
  @ValidateNested()
  imap: BridgeImapConf;
}
