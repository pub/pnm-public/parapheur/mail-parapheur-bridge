import { AbstractPastaCrudService } from "@pasta/back-core";
import { Task } from "../entities/task.entity";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";

export class TaskService extends AbstractPastaCrudService<Task> {
  constructor(@InjectRepository(Task) public readonly repo: Repository<Task>) {
    super();
  }
}
