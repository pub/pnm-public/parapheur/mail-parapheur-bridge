import * as fsp from "fs/promises";
import * as path from "path";
import { HttpStatus, Injectable } from "@nestjs/common";
import * as axios from "axios";

import { InjectLogger, PastaLogger } from "@pasta/back-logger";
import { get } from "lodash";
import { MailBridgeConfig } from "../config/mail-bridge.config";
import { EmailEngineStatusError } from "../errors/error";
import {
  IEmailEngineAttachment,
  IMoveResponse,
  IEmailEngineMailbox,
  IEmailEngineMessageListEntry,
  IEMailEngineTextResponse,
  IEmailEngineAccountState,
  IEmailEngineGetManyAccountsResponse,
  IEmailEngineAccount,
  IEmailEngineSettings,
} from "../interfaces/emailengine";

@Injectable()
export class EmailEngineApiService {
  private readonly axios: axios.AxiosInstance;
  @InjectLogger("bridge", EmailEngineApiService.name)
  logger: PastaLogger;

  constructor(private readonly conf: MailBridgeConfig) {
    this.axios = axios.default.create({ baseURL: this.conf.emailEngineBaseUrl, timeout: 10 * 1000 });
    // this.axios.interceptors.request.use(config => {
    //   console.log(config.baseURL + config.url);
    //   return config;
    // });
  }

  private sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  private throwIfBodyStatus(response: axios.AxiosResponse, message: string) {
    if (get(response, "data.statusCode", HttpStatus.OK) !== HttpStatus.OK) {
      throw new EmailEngineStatusError(message);
    }
    return response;
  }

  /**
   * downloadAttachment
   * @param account the emailengine account name
   * @param attachment the emailengine attachment object
   * @param to absolute path to download the file to
   * @returns the full path to downloaded file
   */
  async downloadAttachment(account: string, attachment: IEmailEngineAttachment, to: string): Promise<string> {
    try {
      this.logger.debug(`downloading attachment ${attachment.filename} to ${to}`, null, { account });
      const response = await this.axios.get(`/account/${account}/attachment/${attachment.id}`, {
        responseType: "stream",
      });
      this.throwIfBodyStatus(response, `Failed to download attachment`);
      const dest = path.join(to, attachment.filename);
      await fsp.writeFile(dest, response.data);
      return dest;
    } catch (e) {
      this.logger.warn(`failed to download attachment ${attachment.filename}`, e, { account });
    }
  }

  // note: moving the message changes all its internal ids (for attachments, texts, and the message itself)
  async moveMessage(account: string, id: string, path: string): Promise<IMoveResponse> {
    this.logger.debug(`move message ${id} to ${path}`, null, { account });
    const response = await this.axios.put(`/account/${account}/message/${id}/move`, { path });
    this.throwIfBodyStatus(response, `Failed to download move message ${id} to ${path} in ${account}`);
    return response.data;
  }

  async getMailboxes(account: string): Promise<IEmailEngineMailbox[]> {
    this.logger.debug(`Get mailboxes for ${account}`);
    const result = await this.axios.get(`/account/${account}/mailboxes`);
    this.throwIfBodyStatus(result, `Failed to list mailboxes for ${account}`);
    return get(result, "data.mailboxes", []);
  }

  async createMailbox(account: string, name: string) {
    this.logger.debug(`create mailbox ${name}`, null, { account });
    const mailboxes = await this.getMailboxes(account);
    const mailbox = mailboxes.find(m => m.name === name);
    if (!mailbox) {
      const response = await this.axios.post(`/account/${account}/mailbox`, { path: [name] });
      this.throwIfBodyStatus(response, `Failed to create mailbox ${name} for ${account}`);
      return response;
    }
  }

  async listMailboxMessages(account: string, mailbox = "INBOX"): Promise<IEmailEngineMessageListEntry[]> {
    const result = await this.axios.get(`/account/${account}/messages`, { params: { path: mailbox } });
    this.throwIfBodyStatus(result, `Failed to list messages for ${account}/${mailbox}`);
    return get(result, "data.messages", []);
  }

  async getText(account: string, textId: string): Promise<IEMailEngineTextResponse> {
    const result = await this.axios.get(`/account/${account}/text/${textId}`);
    this.throwIfBodyStatus(result, `Failed to get text ${textId} for ${account}`);
    return result.data;
  }

  async getMessage(account: string, messageId: string) {
    this.logger.debug(`getMessage ${messageId} for ${account}`, EmailEngineApiService.name, { account, messageId });
    const response = await this.axios.get(`/account/${account}/message/${messageId}`);
    this.throwIfBodyStatus(response, `Failed to get message ${messageId} for ${account}`);
    const message: IEmailEngineMessageListEntry = response.data;
    const text = await this.getText(account, message.text.id);
    return { ...message, text: { ...message.text, ...text } };
  }

  async checkAccount(account: string) {
    try {
      const response = await this.axios.get(`/account/${account}`);
      this.throwIfBodyStatus(response, `Failed to get account ${account}`);
      return true;
    } catch (e) {
      return false;
    }
  }

  async getAccounts(state?: IEmailEngineAccountState): Promise<IEmailEngineGetManyAccountsResponse> {
    const params: Record<string, any> = {};
    if (state) params.state = state;
    const response = await this.axios.get("/accounts", { params });
    this.throwIfBodyStatus(response, `Failed to get accounts`);
    return response.data;
  }

  async waitForAccountConnected(account: string, maxWait = 20 * 1000) {
    this.logger.debug(`Wait for account ${account} to be connected`);
    let success = false;
    const begin = Date.now();
    while (!success) {
      if (Date.now() - begin >= maxWait) throw new Error(`Account ${account} failed to connect within ${maxWait}ms`);
      const { accounts } = await this.getAccounts(IEmailEngineAccountState.CONNECTED);
      if (accounts.find(acc => acc.name === account)) {
        success = true;
      } else {
        this.logger.debug(`Account ${account} still not connected yet, retrying in ${500}ms`);
        await this.sleep(500);
      }
    }
  }

  async createAccount(accountBody: IEmailEngineAccount) {
    this.logger.debug(`create account ${accountBody.name}`);
    if (await this.checkAccount(accountBody.name)) {
      return;
    }

    const response = await this.axios.post(`/account`, accountBody);
    this.throwIfBodyStatus(response, `Failed to create account ${accountBody.name}`);
    return response;
  }

  async deleteAccount(account: string) {
    const response = await this.axios.delete(`/account/${account}`);
    this.throwIfBodyStatus(response, `Failed to delete account ${account}`);
  }

  async deleteMessage(account: string, mailId: string, force = false) {
    const response = await this.axios.delete(`/account/${account}/message/${mailId}?force=${force}`);
    this.throwIfBodyStatus(response, `Failed to delete mail ${mailId} in ${account}`);
  }

  async setSettings(settings: IEmailEngineSettings) {
    this.logger.debug("set settings", null, { settings });
    const response = await this.axios.post(`/settings`, settings);
    this.throwIfBodyStatus(response, `Failed to set settings`);
    return response;
  }
}
