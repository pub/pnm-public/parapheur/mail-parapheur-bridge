import * as fs from "fs";
import PDFDocument from "pdfkit";
import { Injectable } from "@nestjs/common";

@Injectable()
export class PdfService {
  async generatePdf(text: string, filePath: string): Promise<string> {
    return new Promise((resolve, reject) => {
      const doc = new PDFDocument({ size: "A4" });
      const fileStream = fs.createWriteStream(filePath);

      doc.pipe(fileStream);

      fileStream.on("finish", () => {
        resolve(filePath);
      });

      fileStream.on("error", e => {
        reject(e);
      });

      doc.on("error", e => {
        reject(e);
      });

      doc.text(text);
      doc.end();
    });
  }
}
