import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { AbstractPastaCrudService } from "@pasta/back-core";
import { Repository } from "typeorm";
import { Breadcrumb } from "../entities/breadcrumb.entity";

@Injectable()
export class BreadcrumbService extends AbstractPastaCrudService<Breadcrumb> {
  constructor(@InjectRepository(Breadcrumb) public readonly repo: Repository<Breadcrumb>) {
    super();
  }

  async leaveBreadcrumb(taskId: number, message: string): Promise<Breadcrumb> {
    return this.repo.save({
      task: { id: taskId },
      message,
    });
  }
}
