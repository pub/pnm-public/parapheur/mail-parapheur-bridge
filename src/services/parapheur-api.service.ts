import { HttpStatus, Injectable } from "@nestjs/common";
import axios, { AxiosInstance } from "axios";
import fs from "fs";
import FormData from "form-data";
import { ParapheurClientConfig } from "../config/parapheur-client.config";

@Injectable()
export class ParapheurApiService {
  private readonly axios: AxiosInstance;

  private token: string;

  constructor(private readonly config: ParapheurClientConfig) {
    this.axios = axios.create({
      baseURL: this.config.parapheurBaseUrl,
      headers: {
        "Content-Type": "application/json",
      },
    });

    this.axios.interceptors.request.use(async config => {
      if (!this.token && config.url !== "/auth") {
        this.token = await this.getParapheurToken();
      }
      config.headers["Authorization"] = `Bearer ${this.token}`;
      return config;
    });

    this.axios.interceptors.response.use(
      response => response,
      async error => {
        if (error.response.status === HttpStatus.UNAUTHORIZED) {
          this.token = await this.getParapheurToken();
          error.config.headers.Authorization = `Bearer ${this.token}`;
          return axios.request(error.config);
        }
        throw error;
      }
    );
  }

  async getParapheurToken() {
    const { data } = await this.axios.post("/auth", {
      login: this.config.parapheurLogin,
      password: this.config.parapheurPassword,
    });
    return data.accessToken;
  }

  async createLetterFile() {
    const { data } = await this.axios.post("/letter-file?join=folders");
    return data;
  }

  async updateLetterFile(letterFileId: number, body: any) {
    const { data } = await this.axios.patch(`/letter-file/${letterFileId}`, body);
    return data;
  }

  async undraftLetterFile(letterFileId: number) {
    const { data } = await this.axios.post(`/letter-file/${letterFileId}/undraft`);
    return data;
  }

  async cancelCreation(letterFileId: number) {
    const { data } = await this.axios.delete(`/letter-file/${letterFileId}`);
    return data;
  }

  async appendToFlux(letterFileId: number, mail: string) {
    const { data } = await this.axios.post(`/letter-file/${letterFileId}/downstream`, { mail });
    return data;
  }

  async appendToCC(letterFileId: number, mail: string) {
    const { data } = await this.axios.post(`/letter-file/${letterFileId}/cc-user`, { mail });
    return data;
  }

  async createFolder(letterFileId: number, folderName: string) {
    const { data } = await this.axios.post(`/letter-file/${letterFileId}/folder`, { name: folderName });
    return data;
  }

  async createDocument(letterFileId: number, folderId: number, filePath: string) {
    const form = new FormData();
    const documentName = filePath.split("/").pop();
    form.append("file", fs.readFileSync(filePath), documentName);
    const { data } = await this.axios.post(`/file`, form, {
      headers: {
        ...form.getHeaders(),
      },
    });

    const { data: document } = await this.axios.post(`/letter-file/${letterFileId}/document`, {
      name: documentName,
      storageKey: data.key,
      folder: folderId,
    });
    return document;
  }
}
