import { Inject, Injectable } from "@nestjs/common";
import { EmailEngineApiService } from "./emailengine-api.service";
import { BridgeAccountsConfig } from "../config/mail-bridge.config";
import { BridgeMailbox } from "../enums/mailboxes";
import { InjectLogger, PastaLogger } from "@pasta/back-logger";
import { MailProcessorService } from "./mail-processor.service";
import { BridgeValidationError, UnknownBridgeError } from "../errors/error";
import { HedwigeClient } from "hedwige-ts-sdk";
import { HEDWIGE_SYMBOL } from "./hedwige.service";
import { HedwigeConfig } from "../config/hedwige.config";
import { IEmailEngineMessageListEntry } from "../interfaces/emailengine";

@Injectable()
export class PollerService {
  @InjectLogger("bridge", PollerService.name)
  private readonly logger: PastaLogger;

  constructor(
    private readonly emailEngineApiService: EmailEngineApiService,
    private readonly accountsConf: BridgeAccountsConfig,
    private readonly mailProcessorService: MailProcessorService,
    @Inject(HEDWIGE_SYMBOL) private readonly hedwigeService: HedwigeClient,
    private readonly hedwigeConfig: HedwigeConfig
  ) {}

  async poll() {
    this.logger.debug("Polling...");
    for (const account of this.accountsConf.accountsForEmailengine) {
      let mails: IEmailEngineMessageListEntry[];
      try {
        mails = await this.emailEngineApiService.listMailboxMessages(account.name, BridgeMailbox.INBOX);
      } catch (e) {
        this.logger.error("Failed to fetch emails", e, PollerService.name);
        return;
      }
      for (const mail of mails) {
        try {
          await this.mailProcessorService.process({ ...mail, account: account.name });
          await this.emailEngineApiService.moveMessage(account.name, mail.id, BridgeMailbox.DONE);
        } catch (e) {
          this.logger.error(`Error while processing mail ${mail.id} from ${account.name}`, e.stack);

          if (e instanceof BridgeValidationError) {
            if (e.shouldRemove) {
              await this.emailEngineApiService.deleteMessage(account.name, mail.id, true);
            } else {
              await this.emailEngineApiService.moveMessage(account.name, mail.id, BridgeMailbox.ERROR);
            }

            await this.hedwigeService.sendMail({
              from: this.hedwigeConfig.sourceAddress,
              to: mail.from.address,
              subject: "Votre message n'a pas pu être inséré dans le parapheur",
              text: e.publicMessage,
            });
          } else {
            const bridgeError = new UnknownBridgeError(mail.subject);
            await this.emailEngineApiService.moveMessage(account.name, mail.id, BridgeMailbox.ERROR);
            await this.hedwigeService.sendMail({
              from: this.hedwigeConfig.sourceAddress,
              to: mail.from.address,
              subject: "Votre message n'a pas pu être inséré dans le parapheur",
              text: bridgeError.publicMessage,
            });
          }
        }
      }
    }
    this.logger.debug("Polling end...");
  }
}
