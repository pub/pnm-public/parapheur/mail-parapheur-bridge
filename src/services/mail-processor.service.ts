import { HttpStatus, Injectable } from "@nestjs/common";
import { IEmailEngineMessageListEntry } from "../interfaces/emailengine";
import { ParapheurApiService } from "./parapheur-api.service";
import { MailBridgeConfig, BridgeAccountsConfig } from "../config/mail-bridge.config";
import { Task, TaskStatus } from "../entities/task.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import * as path from "path";
import * as fs from "fs";
import { EmailEngineApiService } from "./emailengine-api.service";
import { InjectLogger, PastaLogger } from "@pasta/back-logger";
import { BridgeEmptyFluxError, BridgeInvalidFromError } from "../errors/error";
import validEmails from "../valid-emails.json";
import { PdfService } from "./pdf.service";
import { stripHtml } from "string-strip-html";
import { BreadcrumbService } from "./breadcrumb.service";
import { ParapheurClientConfig } from "../config/parapheur-client.config";

@Injectable()
export class MailProcessorService {
  @InjectLogger("bridge", MailProcessorService.name)
  private readonly logger: PastaLogger;

  validFromRegex = new RegExp(`(${validEmails.patterns.join("|")})`);

  constructor(
    private readonly emailEngineApi: EmailEngineApiService,
    private readonly parapheurService: ParapheurApiService,
    private readonly conf: MailBridgeConfig,
    private readonly parapheurConfig: ParapheurClientConfig,
    private readonly accountsConf: BridgeAccountsConfig,
    private readonly pdfService: PdfService,
    @InjectRepository(Task) private readonly taskRepo: Repository<Task>,
    private readonly breadcrumbService: BreadcrumbService
  ) {}

  getTaskPath(id: number) {
    return path.join(this.conf.baseDir, "tasks", `${id}`);
  }

  async cleanup(id: number) {
    await this.taskRepo.update(id, { status: TaskStatus.DONE });
    fs.rmSync(this.getTaskPath(id), { recursive: true });
  }

  async init(mail: IEmailEngineMessageListEntry & { account: string }) {
    this.logger.debug("Inserting task in database...", null, mail);
    const task = await this.taskRepo.save({
      subject: mail.subject || "null",
      from: mail.from.address,
      status: TaskStatus.PENDING,
    });
    this.logger.debug(`Task ${task.id}} inserted`, null, mail);
    await this.breadcrumbService.leaveBreadcrumb(task.id, "Démarrage");

    if (!this.isValidFrom(mail.from.address)) {
      const error = new BridgeInvalidFromError(mail.subject);
      await this.breadcrumbService.leaveBreadcrumb(task.id, error.breadCrumbMessage);
      throw error;
    }

    this.logger.debug("Downloading attachments...", null, mail);
    await this.breadcrumbService.leaveBreadcrumb(task.id, "Téléchargement des pièces jointes");
    const taskPath = this.getTaskPath(task.id);
    fs.mkdirSync(taskPath, { recursive: true });
    const ignoredExtensions = this.conf.ignoreAttachmentExtensions;
    const attachmentsPaths = await Promise.all(
      (mail.attachments || [])
        .filter(attachment => {
          const extension = path.parse(attachment.filename).ext.toLowerCase().replace(".", "");
          return !ignoredExtensions.includes(extension);
        })
        .map(async attachment => this.emailEngineApi.downloadAttachment(mail.account, attachment, taskPath))
    );
    this.logger.debug(attachmentsPaths.length ? "Attachments downloaded" : "No attachments", null, mail);

    return {
      task,
      taskPath,
      attachmentsPaths,
    };
  }

  async process(mail: IEmailEngineMessageListEntry & { account: string }) {
    this.logger.debug("Processing...", null, mail);
    const { task, attachmentsPaths, taskPath } = await this.init(mail);
    try {
      this.logger.debug("Creating letter file...", null, mail);
      const letterFile: { id: number; chronoNumber: string; folders: { id: number }[] } =
        await this.parapheurService.createLetterFile();
      this.logger.debug(`Letter file ${letterFile.id} [${letterFile.chronoNumber}] created`, null, mail);
      await this.breadcrumbService.leaveBreadcrumb(task.id, `Parapheur ${letterFile.chronoNumber} créé`);
      task.letterfileId = letterFile.id;
      await this.taskRepo.save(task);

      this.logger.debug("Appending flux...", null, mail);
      const flux = await this.getFlux(mail);
      let validFluxElements = 0;
      if (flux.length) {
        await this.breadcrumbService.leaveBreadcrumb(task.id, `Ajout de ${flux.length} éléments au flux`);
      }
      for (const fluxMail of flux) {
        try {
          await this.parapheurService.appendToFlux(letterFile.id, fluxMail);
          this.logger.debug(`${fluxMail} appended to flux`, null, mail);
          validFluxElements++;
          await this.breadcrumbService.leaveBreadcrumb(task.id, `"${fluxMail}" ajouté au flux`);
        } catch (e) {
          await this.breadcrumbService.leaveBreadcrumb(task.id, `Erreur lors de l'ajout au flux de "${fluxMail}"`);
          if (e.response?.status === HttpStatus.BAD_REQUEST) {
            this.logger.warn(e.response.data.message);
          } else {
            throw e;
          }
        }
      }

      const cc = await this.getCC(mail);
      if (cc.length) {
        await this.breadcrumbService.leaveBreadcrumb(task.id, `Ajout de ${cc.length} éléments en copie`);
      }
      for (const ccMail of cc) {
        try {
          await this.parapheurService.appendToCC(letterFile.id, ccMail);
          this.logger.debug(`${ccMail} appended to cc users`, null, mail);
          await this.breadcrumbService.leaveBreadcrumb(task.id, `"${ccMail}" ajouté en copie`);
        } catch (e) {
          await this.breadcrumbService.leaveBreadcrumb(task.id, `Erreur lors de l'ajout en copie de "${ccMail}"`);
          if (e.response?.status === HttpStatus.BAD_REQUEST) {
            this.logger.warn(e.response.data.message);
          } else {
            throw e;
          }
        }
      }

      if (!validFluxElements) {
        const error = new BridgeEmptyFluxError(mail.subject, task.id);
        await this.breadcrumbService.leaveBreadcrumb(task.id, error.breadCrumbMessage);
        throw error;
      }

      const text = await this.emailEngineApi.getText(mail.account, mail.text.id);
      if (text.html || text.plain) {
        const rootFolder = letterFile.folders.at(0);
        this.logger.debug("Uploading mail body...", null, mail);
        await this.breadcrumbService.leaveBreadcrumb(task.id, "Création du pdf à partir du corps du mail");
        const mailPath = path.join(taskPath, "mail.pdf");

        if (text.plain) {
          await this.pdfService.generatePdf(text.plain, mailPath);
        } else {
          await this.pdfService.generatePdf(stripHtml(text.html).result, mailPath);
        }
        await this.breadcrumbService.leaveBreadcrumb(task.id, "Versement du document principal");
        await this.parapheurService.createDocument(letterFile.id, rootFolder.id, mailPath);
      }

      this.logger.debug("Uploading attachments...", null, mail);
      await this.breadcrumbService.leaveBreadcrumb(task.id, "Versement des pièces jointes");
      const attachmentFolder = await this.parapheurService.createFolder(letterFile.id, "Pièces jointes");
      for (const attachmentPath of attachmentsPaths) {
        await this.parapheurService.createDocument(letterFile.id, attachmentFolder.id, attachmentPath);
        await this.breadcrumbService.leaveBreadcrumb(task.id, `Versement de ${attachmentPath} effectué`);
      }

      this.logger.debug("Set letterfile metadata...", null, mail);
      await this.breadcrumbService.leaveBreadcrumb(task.id, "Mise à jour des méta données");
      await this.parapheurService.updateLetterFile(letterFile.id, {
        name: mail.subject,
      });

      this.logger.debug("Undrafting letterfile...", null, mail);
      await this.breadcrumbService.leaveBreadcrumb(task.id, "Sortie du mode brouillon");
      await this.parapheurService.undraftLetterFile(letterFile.id);

      this.logger.debug("Marking task as done...", null, mail);
      await this.breadcrumbService.leaveBreadcrumb(task.id, "Nettoyage et cloture");
      await this.cleanup(task.id);
      await this.breadcrumbService.leaveBreadcrumb(task.id, "Terminé");
    } catch (e) {
      this.logger.error("Error while processing mail", e, null, mail);
      await this.breadcrumbService.leaveBreadcrumb(task.id, "Passage en status erreur");
      await this.taskRepo.update(task.id, { status: TaskStatus.ERROR });
      if (task.letterfileId) {
        await this.breadcrumbService.leaveBreadcrumb(task.id, "Suppression du parapheur");
        await this.parapheurService.cancelCreation(task.letterfileId).catch(e => {
          this.logger.error("Error while cancelling letterfile", e, null, mail);
        });
      }
      throw e;
    }
  }

  async getFlux(mail: IEmailEngineMessageListEntry & { account: string }) {
    const accountMail = this.accountsConf.accountsForEmailengine
      .find(account => account.name === mail.account)
      ?.imap?.auth?.user.toLowerCase();

    const from = mail.from.address;
    const to = this.conf.debugMails.length
      ? this.conf.debugMails
      : mail.to
          .map(to => to.address.toLowerCase())
          .filter(to => to !== accountMail && !this.conf.ignoreMails.includes(to));

    return [from, ...to];
  }

  async getCC(mail: IEmailEngineMessageListEntry & { account: string }) {
    const accountMail = this.accountsConf.accountsForEmailengine.find(account => account.name === mail.account)?.imap
      ?.auth?.user;

    return mail.cc?.map(cc => cc.address).filter(cc => cc !== accountMail) || [];
  }

  isValidFrom(from: string) {
    return this.validFromRegex.test(from);
  }
}
