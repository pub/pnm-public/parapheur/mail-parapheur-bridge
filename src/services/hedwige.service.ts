import { FactoryProvider } from "@nestjs/common";
import { HedwigeClient } from "hedwige-ts-sdk";
import { HedwigeConfig } from "../config/hedwige.config";

export const HEDWIGE_SYMBOL = Symbol("HedwigeClient");

export const HedwigeService: FactoryProvider<HedwigeClient> = {
  provide: HEDWIGE_SYMBOL,
  useFactory: (config: HedwigeConfig) => {
    return new HedwigeClient({
      clientId: config.clientId,
      clientSecret: config.clientSecret,
      version: config.version,
    });
  },
  inject: [HedwigeConfig],
};
