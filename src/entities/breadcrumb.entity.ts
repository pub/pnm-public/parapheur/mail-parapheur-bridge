import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, ManyToOne } from "typeorm";
import { Task } from "./task.entity";

@Entity({ orderBy: { creationDate: "DESC" } })
export class Breadcrumb extends PastaBaseEntity {
  @Column()
  message: string;

  @ManyToOne(() => Task, task => task.breadcrumbs)
  task: Task;
}
