import { PastaBaseEntity } from "@pasta/back-core";
import { Column, Entity, OneToMany } from "typeorm";
import { Breadcrumb } from "./breadcrumb.entity";

export enum TaskStatus {
  PENDING = "pending",
  DONE = "done",
  ERROR = "error",
  INVALID = "invalid",
}

@Entity()
export class Task extends PastaBaseEntity {
  @Column()
  from: string;

  @Column()
  status: TaskStatus;

  @Column()
  subject: string;

  @Column({ nullable: true })
  letterfileId: number;

  @OneToMany(() => Breadcrumb, bc => bc.task)
  breadcrumbs: Breadcrumb[];
}
