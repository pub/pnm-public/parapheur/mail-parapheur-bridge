import { EmailEngineEventType, IEmailEngineAttachment, IEmailEngineCorrespondant, IEMailEngineTextInfo, IEMailEngineTextResponse } from ".";

export interface IEmailEngineWebHookBody {
  account: string;
  date?: string; // "2021-10-26T13:00:07.331Z"
  path: string;
  specialUse?: string;
  event?: EmailEngineEventType;
  data: {
    id: string;
    uid: number;
    date: string;
    flags?: string[]; // ["\\Recent"]
    unseen?: boolean;
    size: number;
    subject: string;
    from: IEmailEngineCorrespondant;
    replyTo?: IEmailEngineCorrespondant;
    sender?: IEmailEngineCorrespondant;
    to: IEmailEngineCorrespondant[];
    attachments?: IEmailEngineAttachment[];
    messageId: string; // "<839C165A-46B2-42E9-9DF4-3BCCA79353B3@me.test>"
    headers?: {
      "return-path": string[]; // ["<kevin@me.test>"],
      "delivered-to": string[]; // ["debug@me.test"],
      "received": string[];
      // example for received:
      /*[
        "from b0921b3dccfe by b0921b3dccfe (Dovecot) with LMTP id tcsaAX7Bd2FnAQAAyIsA0w for <debug@me.test>; Tue, 26 Oct 2021 08:51:10 +0000",
        "from [IPv6:::1] (unknown [172.27.0.1]) by b0921b3dccfe (Postfix) with ESMTPS id 003E52A66DC for <debug@localhost>; Tue, 26 Oct 2021 08:51:09 +0000 (UTC)"
      ],*/
      "from": string[]; // ["Kevin Roulleau <kevin@me.test>"],
      "content-type": string[]; // ["text/plain; charset=us-ascii"],
      "content-transfer-encoding"?: string[]; // ["quoted-printable"],
      "mime-version": string[]; // ["1.0 (Mac OS X Mail 13.4 \\(3608.120.23.2.4\\))"],
      "subject": string[]; // ["Test2"],
      "message-id": string[]; // ["<839C165A-46B2-42E9-9DF4-3BCCA79353B3@me.test>"],
      "date": string[]; // ["Tue, 26 Oct 2021 10:51:09 +0200"],
      "to": string[]; // ["debug@localhost"],
      "x-mailer": string[]; // ["Apple Mail (2.3608.120.23.2.4)"]
    };
    text: IEMailEngineTextInfo & IEMailEngineTextResponse;
  };
}
