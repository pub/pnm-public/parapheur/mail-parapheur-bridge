export interface IEmailEngineCorrespondant {
  name: string; // Kevin Roulleau
  address: string; // kevin@me.test
}

export interface IEMailEngineEncodedSize {
  plain?: number;
  html?: number;
}

export interface IEMailEngineTextInfo {
  id: string; // "AAAAAQAAAASTkaExkJA",
  encodedSize: IEMailEngineEncodedSize;
}

export interface IEMailEngineTextResponse {
  plain?: string;
  html?: string;
  hasMore: boolean;
}

export interface IEmailEngineAttachment {
  id: string;
  contentType: string; // "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
  encodedSize: number;
  filename: string;
  embedded: boolean;
  inline: boolean;
}

export enum EmailEngineEventType {
  MESSAGE_NEW = "messageNew",
  MESSAGE_DELETED = "messageDeleted",
  MESSAGE_UPDATED = "messageUpdated",
  MESSAGE_SENT = "messageSent",
  MESSAGE_FAILED = "messageFailed",
  MESSAGE_BOUNCE = "messageBounce",
  MAILBOX_RESET = "mailboxReset",
  MAILBOX_DELETED = "mailboxDeleted",
  MAILBOX_NEW = "mailboxNew",
  AUTHENTICATION_ERROR = "authenticationError",
  AUTHENTICATION_SUCCESS = "authenticationSuccess",
  CONNECT_ERROR = "connectError",
  FALSE = "false",
}

export interface IMoveResponse {
  id: string;
  path: string;
  uid: number;
}

export interface IEmailEngineAccount {
  account: string;
  name: string;
  imap: {
    auth: {
      user: string;
      pass: string;
    };
    host: string;
    port: number;
    secure: boolean;
    tls: {
      rejectUnauthorized: boolean;
    };
    resyncDelay: number;
  };
  smtp: false;
  oauth2: false;
}

export interface IEmailEngineSettings {
  webhooks: string;
  webhookEvents: EmailEngineEventType[];
  notifyText: boolean;
}

export enum IEmailEngineAccountState {
  INIT = "init",
  CONNECTING = "connecting",
  CONNECTED = "connected",
  AUTHENTICATIONERROR = "authenticationError",
  CONNECTERROR = "connectError",
  UNSET = "unset",
  DISCONNECTED = "disconnected",
}

export interface IEmailEngineGetManyAccountsResponse {
  accounts: {
    account: string;
    lastError: string | null;
    name: string;
    state: IEmailEngineAccountState;
    syncTime: Date;
  }[];
  page: number;
  pages: number;
  total: number;
}

export interface IEmailEngineMailbox {
  path: string;
  listed: boolean;
  name: string;
  specialUse: string;
  subscribed: boolean;
  messages: number;
  uidNext: number;
}

export interface IEmailEngineMessageListEntry {
  id: string;
  uid: number;
  date: string;
  unseen: boolean;
  size: number;
  subject: string;
  from: IEmailEngineCorrespondant;
  to: IEmailEngineCorrespondant[];
  attachments: IEmailEngineAttachment[];
  messageId: string;
  text: IEMailEngineTextInfo & IEMailEngineTextResponse;
  cc: IEmailEngineCorrespondant[];
}
