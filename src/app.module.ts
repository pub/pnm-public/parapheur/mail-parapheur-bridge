import { Module, OnModuleInit } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { PastaAppModule, PastaConfigModule, PastaCrudModule, PastaDbModule } from "@pasta/back-core";
import { LoggerModule } from "@pasta/back-logger";
import { MailBridgeConfig, BridgeAccountsConfig } from "./config/mail-bridge.config";
import { EmailEngineApiService } from "./services/emailengine-api.service";
import { BridgeMailbox } from "./enums/mailboxes";
import { timeout } from "./utils";
import { PollerService } from "./services/poller.service";
import { MailProcessorService } from "./services/mail-processor.service";
import { ParapheurApiService } from "./services/parapheur-api.service";
import { Task } from "./entities/task.entity";
import * as fs from "fs";
import { HedwigeConfig } from "./config/hedwige.config";
import { HedwigeService } from "./services/hedwige.service";
import { ParapheurClientConfig } from "./config/parapheur-client.config";
import { PdfService } from "./services/pdf.service";
import { TaskService } from "./services/task.service";
import { TaskController } from "./controllers/task.controller";
import { Breadcrumb } from "./entities/breadcrumb.entity";
import { BreadcrumbService } from "./services/breadcrumb.service";
@Module({
  imports: [
    PastaCrudModule,
    LoggerModule.register(),
    PastaConfigModule.register(MailBridgeConfig, BridgeAccountsConfig, HedwigeConfig, ParapheurClientConfig),
    PastaDbModule.forRoot(),
    PastaDbModule.forFeature(Task, Breadcrumb),
  ],
  controllers: [AppController, TaskController],
  providers: [
    AppService,
    EmailEngineApiService,
    PollerService,
    MailProcessorService,
    ParapheurApiService,
    HedwigeService,
    PdfService,
    TaskService,
    BreadcrumbService,
  ],
})
export class AppModule extends PastaAppModule implements OnModuleInit {
  constructor(
    private readonly emailEngineApi: EmailEngineApiService,
    private readonly accountsConf: BridgeAccountsConfig,
    private readonly pollerService: PollerService,
    private readonly bridgeConf: MailBridgeConfig
  ) {
    super();
  }

  async init() {
    if (!fs.existsSync(this.bridgeConf.baseDir)) {
      fs.mkdirSync(this.bridgeConf.baseDir, { recursive: true });
    }
    for (const account of this.accountsConf.accountsForEmailengine) {
      await this.emailEngineApi.createAccount(account);
      await this.emailEngineApi.waitForAccountConnected(account.name);
      await this.emailEngineApi.createMailbox(account.name, BridgeMailbox.DONE);
      await this.emailEngineApi.createMailbox(account.name, BridgeMailbox.ERROR);
    }
    return true;
  }

  async onModuleInit() {
    if (global.PASTA_IS_CLI) {
      return;
    }

    let exponentialBackoff = 1;
    let success = false;
    while (!success) {
      try {
        success = await this.init();
      } catch (e) {
        this.logger.error("Cannot initialize EmailEngine configuration", e.message);
        exponentialBackoff *= 2;
        await timeout(exponentialBackoff * 1000);
      }
    }

    setTimeout(async () => {
      // eslint-disable-next-line no-constant-condition
      while (true) {
        await this.pollerService.poll();
        await timeout(5000);
      }
    }, 1000);
  }
}
