export enum BridgeMailbox {
  DONE = "done",
  ERROR = "error",
  INBOX = "INBOX",
}
