import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { useContainer } from "class-validator";
import { LoggerService } from "@pasta/back-logger";

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { logger: LoggerService.getInstance() });
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  app.enableCors();
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
