import { EnvPrefix, FromEnv } from "@pasta/back-core";
import { IsString } from "class-validator";

@EnvPrefix("PARAPHEUR_")
export class ParapheurClientConfig {
  @FromEnv("BASEURL")
  @IsString()
  parapheurBaseUrl: string;

  @FromEnv("LOGIN")
  @IsString()
  parapheurLogin: string;

  @FromEnv("PASSWORD")
  @IsString()
  parapheurPassword: string;
}
