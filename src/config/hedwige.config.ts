import { EnvPrefix, FromEnv } from "@pasta/back-core";
import { IsString } from "class-validator";

@EnvPrefix("HEDWIGE_")
export class HedwigeConfig {
  @FromEnv("CLIENT_ID")
  @IsString()
  clientId: string;

  @FromEnv("CLIENT_SECRET")
  @IsString()
  clientSecret: string;

  @FromEnv("SOURCE_ADDRESS")
  @IsString()
  sourceAddress: string;

  @FromEnv("VERSION")
  @IsString()
  version: string;
}
