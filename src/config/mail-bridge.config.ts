import { EnvPrefix, FromEnv, FromFilePath, SplitStringPipe, UsePipeConf } from "@pasta/back-core";
import { Transform, Type } from "class-transformer";
import { IsBoolean, IsOptional, IsString, ValidateNested } from "class-validator";
import { BridgeAccount } from "../models/bridge-account.model";
import { IEmailEngineAccount } from "../interfaces/emailengine";
import { isNil } from "lodash";

@EnvPrefix("MAIL_BRIDGE_")
export class MailBridgeConfig {
  @FromEnv("EMAILENGINE_BASEURL")
  @IsString()
  emailEngineBaseUrl: string = "http://127.0.0.1:4000/v1";

  @FromEnv("BASE_DIR")
  @IsString()
  baseDir: string = "/tmp/mail-bridge";

  @FromEnv("USE_PROXY")
  @IsBoolean()
  useProxy: boolean = false;

  @FromEnv("PROXY_REJECT_UNAUTHORIZED")
  @IsBoolean()
  proxyRejectUnauthorized: boolean = true;

  @FromEnv("DEBUG_MAILS")
  @IsString({ each: true })
  @IsOptional()
  @UsePipeConf(new SplitStringPipe(","))
  debugMails: string[] = [];

  @FromEnv("IGNORE_MAILS")
  @IsString({ each: true })
  @IsOptional()
  @UsePipeConf(new SplitStringPipe(","))
  ignoreMails: string[] = [];

  @FromEnv("IGNORE_ATTACHMENT_EXTENSIONS")
  @IsString({ each: true })
  @IsOptional()
  @Transform(({ value }) => value.toLowerCase())
  @UsePipeConf(new SplitStringPipe(","))
  ignoreAttachmentExtensions: string[] = ["gif"];
}

@FromFilePath("./accounts.yaml")
export class BridgeAccountsConfig {
  @Type(() => BridgeAccount)
  @ValidateNested({ each: true })
  accounts: BridgeAccount[];

  get accountsForEmailengine(): IEmailEngineAccount[] {
    return this.accounts.map(account => {
      return {
        account: account.name,
        name: account.name,
        imap: {
          auth: { user: account.imap.user, pass: account.imap.password || process.env[account.imap.passwordEnv] },
          host: account.imap.host,
          port: account.imap.port,
          secure: isNil(account.imap.secure) ? true : account.imap.secure,
          resyncDelay: account.imap.resyncDelay || 900,
          tls: {
            rejectUnauthorized: isNil(account.imap.rejectUnauthorized) ? true : account.imap.rejectUnauthorized,
          },
        },
        oauth2: false,
        smtp: false,
      };
    });
  }
}
