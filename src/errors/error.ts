export class EmailEngineStatusError extends Error {}

export abstract class BridgeProcessError extends Error {
  abstract get breadCrumbMessage(): string;
}

export abstract class BridgeValidationError extends BridgeProcessError {
  abstract get breadCrumbMessage(): string;

  taskId: number;
  mailSubject: string;
  shouldRemove = false;

  get message() {
    return `Task ${this.taskId} validation error`;
  }

  protected get publicMessagePrefix() {
    return `
    Bonjour,
    Votre mail "${this.mailSubject}" a bien été reçu pour insertion dans l'application Parapheur.
    Cependant`;
  }

  get publicMessage() {
    return "Le mail n'a pas pu être traité correctement car il ne respecte pas le format attendu.";
  }

  constructor(mailSubject: string, taskId?: number) {
    super();
    this.taskId = taskId;
    this.mailSubject = mailSubject;
  }
}

export class UnknownBridgeError extends BridgeValidationError {
  get breadCrumbMessage(): string {
    return "not needed";
  }

  get publicMessage(): string {
    return `
    ${this.publicMessagePrefix} une erreur inattendue est survenue lors de l'insertion de votre message dans le parapheur.
    Veuillez contacter le support technique.
    `;
  }
}

export class BridgeEmptyFluxError extends BridgeValidationError {
  get message() {
    return `Task validation error: no valid flux mail`;
  }

  get publicMessage() {
    return `
    ${this.publicMessagePrefix} l'opération a échoué car aucun utilisateur n'a pu être trouvé pour recevoir le courrier.
    Merci de vérifier que les destinataires renseignés dans le mail sont bien valide
    `;
  }

  get breadCrumbMessage() {
    return "Aucun utilisateur n'a pu être trouvé pour recevoir le courrier";
  }
}

export class BridgeInvalidFromError extends BridgeValidationError {
  shouldRemove = true;

  get message() {
    return `Task validation error: invalid from`;
  }

  get publicMessage() {
    return `
    ${this.publicMessagePrefix} l'opération a échoué car l'adresse mail de l'expéditeur n'est pas autorisée.
    Merci de réessayer en utilisant une adresse mail autorisée.
    `;
  }

  get breadCrumbMessage() {
    return "L'adresse mail de l'expéditeur n'est pas autorisée";
  }
}
