import { Controller } from "@nestjs/common";
import { AbstractPastaCrudController, PastaCrud } from "@pasta/back-core";
import { Task } from "../entities/task.entity";
import { TaskService } from "../services/task.service";

@PastaCrud({
  model: Task,
  routes: { exclude: ["createOne", "updateOne", "deleteOne"] },
  joins: { breadcrumbs: {} },
  sort: { default: ["-creationDate"] },
})
@Controller("/task")
export class TaskController implements AbstractPastaCrudController<Task> {
  constructor(public readonly service: TaskService) {}
}
